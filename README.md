## Conan package recipe for [*libxslt*](http://xmlsoft.org/XSLT/)

libxslt is a software library implementing XSLT processor, based on libxml2. It also implements most of the EXSLT set
of processor-portable extensions functions and some of Saxon's evaluate and expressions extensions.


## Conan Recipe License

NOTE: The conan recipe license applies only to the files of this recipe, which can be used to build and package libxml2.
It does *not* in any way apply or is related to the actual software being packaged.


ORIGIN: [bincrafters/conan-libxslt](https://github.com/bincrafters/conan-libxslt)
